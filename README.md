# Overview
This module tries to provide a middle ground between the `list_string` field type and an `entity_reference` field type pointing to a taxonomy term.
The problem with string lists is that they are terribly inflexible which tends to cause a _lot_ of legacy naming syndrome in systems that are larger and maintained over a long time.
Taxonomy term references on the other hand have the problem that what they are referencing is _content_ which makes it very hard to manage if there is a relatively tight integration with templates.

A good use case for Pickable Things would be a field where you can select some-_thing_ that should result in a CSS class being added to the template of your entity.
You have to define the styles to match the things that can be selected, so defining the values as configuration makes the deployment and update logistics much easier;
but having a string list field is pretty much equivalent to hard-coding the classes.

Pickable Things provide a level of abstraction between the two extremes. You pick a _configuration_ entity which contains a **value** field that you can use for constructing a class name.
The configuration entity can be deployed easily and future changes to the class names are trivial as well.

# How to use Pickable Things
First, you define some pickable things. Go to `/admin/structure/pickable_thing_entity` and create some entities.
You will have to define a value and a group name for each of them - the group name is what controls which pickable things belong together.

Next, you add a new entity reference field to one of your content types and configure it to reference the pickable thing config entity.
In the form display configuration, select the pickable thing widget and configure its group name to match one of the group names you used earlier when creating the things.

This is pretty much it. You can now select your _things_ and use them in your templates to construct class names or whatever you find useful.

## APIs
If you need / want to work with pickable things on the code level, there is a simple manager service available to fetch
pickable things. The service has the ID `pickable_config_thing.manager` and it implements the `IPickableThingManager`
interface. See the code comments on that class for more information.
