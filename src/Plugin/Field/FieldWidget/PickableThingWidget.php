<?php

namespace Drupal\pickable_config_thing\Plugin\Field\FieldWidget;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pickable_config_thing\Entity\PickableThingEntity;


/**
 * Plugin implementation of the 'pickable_thing_widget' widget.
 *
 * @FieldWidget(
 *   id = "pickable_thing_widget",
 *   label = @Translation("Pickable thing widget"),
 *   field_types = {
 *     "entity_reference"
 *   },
 *   multiple_values = TRUE
 * )
 */
class PickableThingWidget extends OptionsSelectWidget
{

	/**
	 * {@inheritdoc}
	 */
	public static function defaultSettings()
	{
		return [
				'group' => '',
			] + parent::defaultSettings();
	}

	/**
	 * {@inheritdoc}
	 */
	public function settingsForm(array $form, FormStateInterface $form_state)
	{
		$elements = [];

		$elements['group'] = [
			'#type' => 'textfield',
			'#title' => t('PickableThing group'),
			'#default_value' => $this->getSetting('group'),
			'#required' => true,
		];

		return $elements;
	}

	/**
	 * {@inheritdoc}
	 */
	public function settingsSummary()
	{
		$summary = [];

		$summary[] = t('Picking things from group: @group', ['@group' => $this->getSetting('group')]);

		return $summary;
	}

	/**
	 * Returns the array of pickable thing options.
	 *
	 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
	 *   The entity for which to return options.
	 *
	 * @return array
	 *   The array of options for the widget.
	 */
	protected function getOptions(FieldableEntityInterface $entity) {
		if (!isset($this->options)) {
			$options = [];
			$targetType = $this->fieldDefinition->getItemDefinition()->getSetting('target_type');

			// Limit the settable options for the current user account.
			if ($targetType === 'pickable_thing_entity') {
				$options = \Drupal::service('pickable_config_thing.manager')->getOptionsByGroup($this->getSetting('group'), $this->getEmptyLabel());
			}

			$this->options = $options;
		}
		return $this->options;
	}

}
