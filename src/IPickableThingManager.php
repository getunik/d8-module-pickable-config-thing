<?php

namespace Drupal\pickable_config_thing;


use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\pickable_config_thing\Entity\PickableThingEntityInterface;


interface IPickableThingManager
{
	/**
	 * Gets an array of options for pickable things in the given group.
	 *
	 * @param string $group
	 *   The pickable thing group for which options should be returned.
	 * @param string|TranslatableMarkup|null $emptyLabel
	 *   The label for the _empty_ option.
	 *
	 * @return array
	 *   The options array.
	 */
	public function getOptionsByGroup($group, $emptyLabel = NULL);

	/**
	 * Gets the pickable thing identified by the given ID.
	 *
	 * @param string $id
	 *   The pickable thing ID.
	 *
	 * @return PickableThingEntityInterface|null
	 */
	public function getThingById($id);
}
