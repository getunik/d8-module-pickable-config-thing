<?php

namespace Drupal\pickable_config_thing;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\pickable_config_thing\Entity\PickableThingEntity;


/**
 * Helper service for working with pickable things.
 */
class PickableThingManager implements IPickableThingManager
{
	/**
	 * @var \Drupal\Core\Entity\EntityStorageInterface
	 */
	protected $storage;

	public function __construct(EntityTypeManagerInterface $entityTypeManager)
	{
		$this->storage = $entityTypeManager->getStorage('pickable_thing_entity');
	}

	/**
	 * {@inheritdoc}
	 */
	public function getOptionsByGroup($group, $emptyLabel = NULL)
	{
		$options = [];

		$entities = $this->storage->loadByProperties([
			'group' => $group,
		]);

		usort($entities, [PickableThingEntity::class, 'sort']);

		foreach ($entities as $pickable) {
			$options[$pickable->id()] = $pickable->label();
		}

		// Add an empty option if the widget needs one.
		if (!empty($emptyLabel)) {
			$options = ['_none' => $emptyLabel] + $options;
		}

		return $options;
	}

	/**
	 * {@inheritdoc}
	 */
	public function getThingById($id)
	{
		return $this->storage->load($id);
	}
}
