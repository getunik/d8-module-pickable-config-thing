<?php

namespace Drupal\pickable_config_thing\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\pickable_config_thing\Entity\PickableThingEntityInterface;


/**
 * Class PickableThingEntityForm.
 *
 * @package Drupal\pickable_config_thing\Form
 */
class PickableThingEntityForm extends EntityForm
{

	/**
	 * {@inheritdoc}
	 */
	public function form(array $form, FormStateInterface $form_state)
	{
		$form = parent::form($form, $form_state);

		/** @var PickableThingEntityInterface $pickable_thing_entity */
		$pickable_thing_entity = $this->entity;

		$form['label'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Label'),
			'#maxlength' => 255,
			'#default_value' => $pickable_thing_entity->label(),
			'#description' => $this->t("Label for the Pickable thing entity."),
			'#required' => true,
		];

		$form['id'] = [
			'#type' => 'machine_name',
			'#default_value' => $pickable_thing_entity->id(),
			'#machine_name' => [
				'exists' => '\Drupal\pickable_config_thing\Entity\PickableThingEntity::load',
			],
			'#disabled' => !$pickable_thing_entity->isNew(),
		];

		$form['value'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Value'),
			'#maxlength' => 255,
			'#default_value' => $pickable_thing_entity->getValue(),
			'#description' => $this->t("Value for the Pickable thing entity."),
			'#required' => true,
		];

		$form['group'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Group'),
			'#maxlength' => 255,
			'#default_value' => $pickable_thing_entity->getGroup(),
			'#description' => $this->t("Group for the Pickable thing entity."),
			'#required' => true,
		];

		$form['weight'] = [
			'#type' => 'number',
			'#title' => $this->t('Weight'),
			'#default_value' => $pickable_thing_entity->getWeight(),
			'#description' => $this->t('This affects the order in which pickable things will be available.'),
		];

		return $form;
	}

	/**
	 * {@inheritdoc}
	 */
	public function save(array $form, FormStateInterface $form_state)
	{
		$pickable_thing_entity = $this->entity;
		$status = $pickable_thing_entity->save();

		switch ($status) {
			case SAVED_NEW:
				drupal_set_message($this->t('Created the %label Pickable thing entity.', [
					'%label' => $pickable_thing_entity->label(),
				]));
				break;

			default:
				drupal_set_message($this->t('Saved the %label Pickable thing entity.', [
					'%label' => $pickable_thing_entity->label(),
				]));
		}
		$form_state->setRedirectUrl($pickable_thing_entity->toUrl('collection'));
	}

}
