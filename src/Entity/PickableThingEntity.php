<?php

namespace Drupal\pickable_config_thing\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;


/**
 * Defines the Pickable thing entity entity.
 *
 * @ConfigEntityType(
 *   id = "pickable_thing_entity",
 *   label = @Translation("Pickable thing entity"),
 *   handlers = {
 *     "list_builder" = "Drupal\pickable_config_thing\PickableThingEntityListBuilder",
 *     "form" = {
 *       "add" = "Drupal\pickable_config_thing\Form\PickableThingEntityForm",
 *       "edit" = "Drupal\pickable_config_thing\Form\PickableThingEntityForm",
 *       "delete" = "Drupal\pickable_config_thing\Form\PickableThingEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\pickable_config_thing\PickableThingEntityHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "pickable_thing_entity",
 *   admin_permission = "administer pickable thing config entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/pickable_thing_entity/{pickable_thing_entity}",
 *     "add-form" = "/admin/structure/pickable_thing_entity/add",
 *     "edit-form" = "/admin/structure/pickable_thing_entity/{pickable_thing_entity}/edit",
 *     "delete-form" = "/admin/structure/pickable_thing_entity/{pickable_thing_entity}/delete",
 *     "collection" = "/admin/structure/pickable_thing_entity"
 *   }
 * )
 */
class PickableThingEntity extends ConfigEntityBase implements PickableThingEntityInterface
{

	/**
	 * The Pickable thing entity ID.
	 *
	 * @var string
	 */
	protected $id;

	/**
	 * The Pickable thing entity label.
	 *
	 * @var string
	 */
	protected $label;

	/**
	 * @var mixed.
	 */
	protected $value;

	/**
	 * @var string
	 */
	protected $group;

	/**
	 * @var integer
	 */
	protected $weight;

	/**
	 * @var array
	 */
	protected $data;

	/**
	 * @inheritdoc
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @inheritdoc
	 */
	public function setValue($value)
	{
		$this->value = $value;
	}

	/**
	 * @inheritdoc
	 */
	public function getGroup()
	{
		return $this->group;
	}

	/**
	 * @inheritdoc
	 */
	public function setGroup($group)
	{
		$this->group = $group;
	}

	/**
	 * @inheritdoc
	 */
	public function getWeight()
	{
		return $this->weight;
	}

	/**
	 * @inheritdoc
	 */
	public function setWeight($weight)
	{
		$this->weight = $weight;
	}

	/**
	 * @inheritdoc
	 */
	public function getData()
	{
		return $this->data;
	}

	/**
	 * @inheritdoc
	 */
	public function setData($data)
	{
		$this->data = $data;
	}

	/**
	 * Helper callback for uasort() to sort configuration entities by weight and label.
	 */
	public static function sort(ConfigEntityInterface $a, ConfigEntityInterface $b) {
		$group = strnatcasecmp($a->getGroup(), $b->getGroup());

		if ($group === 0) {
			return ($a->getWeight() < $b->getWeight()) ? -1 : 1;
		}

		return $group;
	}
}
