<?php

namespace Drupal\pickable_config_thing\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;


/**
 * Provides an interface for defining Pickable thing entity entities.
 */
interface PickableThingEntityInterface extends ConfigEntityInterface
{
	/**
	 * Gets the thing's value.
	 *
	 * @return mixed
	 */
	public function getValue();

	/**
	 * Sets the thing's value.
	 *
	 * @param mixed $value
	 */
	public function setValue($value);

	/**
	 * Gets the thing's group.
	 *
	 * @return string
	 */
	public function getGroup();

	/**
	 * Sets the thing's group.
	 *
	 * @param string $group
	 */
	public function setGroup($group);

	/**
	 * Gets the weight of this pickable thing.
	 *
	 * @return integer
	 */
	public function getWeight();

	/**
	 * Sets the weight of this pickable thing.
	 *
	 * @param integer $weight
	 *   The weight to set the thing to.
	 */
	public function setWeight($weight);

	/**
	 * Gets the thing's meta data.
	 *
	 * @return array
	 */
	public function getData();

	/**
	 * Sets the thing's meta data.
	 * @param array $data
	 */
	public function setData($data);
}
