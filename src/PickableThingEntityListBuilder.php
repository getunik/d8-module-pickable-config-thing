<?php

namespace Drupal\pickable_config_thing;

use Drupal\Core\Config\Entity\ConfigEntityListBuilder;
use Drupal\Core\Entity\EntityInterface;


/**
 * Provides a listing of Pickable thing entity entities.
 */
class PickableThingEntityListBuilder extends ConfigEntityListBuilder
{

	/**
	 * {@inheritdoc}
	 */
	public function buildHeader()
	{
		$header['label'] = $this->t('Pickable thing entity');
		$header['id'] = $this->t('Machine name');
		$header['group'] = $this->t('Group');
		$header['weight'] = $this->t('Weight');
		return $header + parent::buildHeader();
	}

	/**
	 * {@inheritdoc}
	 */
	public function buildRow(EntityInterface $entity)
	{
		$row['label'] = $entity->label();
		$row['id'] = $entity->id();
		$row['group'] = $entity->getGroup();
		$row['weight'] = $entity->getWeight();

		return $row + parent::buildRow($entity);
	}

}
